package the.tt.moscow.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import the.tt.moscow.lib.StringList;

/**
 * Created by SPanin on 10/13/2016.
**/
public final class TextAddapter extends BaseAdapter {
    private LayoutInflater lInflater = null;
    private Context m_context = null;
    private StringList m_items = new StringList();
    private int m_resourceid;
    private List<Integer> m_ids = null;

    public TextAddapter(Context context, int resourceid, Integer[] objects, StringList items) {
        m_resourceid = resourceid;
        m_ids = Arrays.asList(objects);
        m_context = context;
        m_items = items;
        lInflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public TextAddapter(Context context, int resourceid, Integer[] objects) {
        m_resourceid = resourceid;
        m_ids = Arrays.asList(objects);
        m_context = context;
        lInflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return m_items.size();
    }
    @Override
    public Object getItem(int position) {
        return m_items.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null)
            view = lInflater.inflate(m_resourceid, parent, false);

        if(m_items == null)
            return view;

        for(int i = 0; i != m_ids.size(); ++i) {
            final String text = m_items.get(position).get(i);
            final int id = m_ids.get(i);
            ((TextView) view.findViewById(id)).setText(text);
        }

        return view;
    }
}
