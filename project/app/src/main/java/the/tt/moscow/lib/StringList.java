package the.tt.moscow.lib;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by SPanin on 10/17/2016.
**/
public final class StringList implements Iterable<ArrayList<String>> {
    private List<ArrayList<String>> m_items = new ArrayList<ArrayList<String>>();

    public final void add(String... items) {
        ArrayList<String> result = new ArrayList<String>();

        for(String s: items)
            result.add(s);

        m_items.add(result);
    }
    public final ArrayList<String> get(int index) {
        return m_items.get(index);
    }
    public final int size() {
        return m_items.size();
    }
    public final boolean isEmpty() {
        return m_items.isEmpty();
    }

    @Override
    public Iterator<ArrayList<String>> iterator() {
        return new StringListIterator(m_items);
    }
    public class StringListIterator implements Iterator<ArrayList<String>> {
        private int m_index = 0;
        private List<ArrayList<String>> m_items = null;

        public StringListIterator(List<ArrayList<String>> items) {
            m_items = items;
        }
        @Override
        public boolean hasNext() {
            return m_index < m_items.size();
        }
        @Override
        public ArrayList<String> next() throws NoSuchElementException{
            if (m_index >= m_items.size())
                throw new NoSuchElementException();

            return m_items.get(m_index++);
        }
        @Override
        public void remove() {
            m_items.remove(m_index);
        }
    }
}
