package the.tt.moscow.db;
import android.content.ContentValues;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by SPanin on 10/5/2016.
**/
public class Match {
    private int m_id;
    private int m_1;
    private int m_2;
    private long m_ts;

    private final static long current() {
        return System.currentTimeMillis();
    }
    public Match(int id, int _1, int _2, long ts) {
        this.m_id = id;
        this.m_1 = _1;
        this.m_2 = _2;
        this.m_ts = ts;
    }
    public Match(int id, int _1, int _2) {
        this(id, _1, _2, current());
    }
    public boolean isValid() {
        return m_id != 0
            && m_1 != 0
            && m_2 != 0
            && m_ts != 0
        ;
    }
    public void setId(int id) {
        this.m_id = id;
    }
    public int id() {
        return m_id;
    }
    public void setPlayer1(int _1) {
        this.m_1 = _1;
    }
    public int player1() {
        return m_1;
    }
    public void setPlayer2(int _2) {
        this.m_2 = _2;
    }
    public int player2() {
        return m_2;
    }
    public void setTs(long ts) {
        this.m_ts = ts;
    }
    public long ts() {
        return m_ts;
    }
    public GregorianCalendar date() {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(new Date(m_ts));
        return cal;
    }
    public void setDate(int year, int month, int day, int hour, int minute) {
        GregorianCalendar cal = new GregorianCalendar(year, month, day, hour, minute);
        m_ts = cal.getTimeInMillis();
    }
    public int getDay() {
        return date().get(Calendar.DATE);
    }
    public int getMonth() {
        return date().get(Calendar.MONTH);
    }
    public int getYear() {
        return date().get(Calendar.YEAR);
    }
    @Override
    public String toString() {
        return getClass().getName() + ": {"
                + "id: " + m_id + ", "
                + "_1: " + m_1 + ", "
                + "_2: " + m_2 + ", "
                + "ts: " + m_ts
                + "}";
    }
    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(o == this) return true;
        if(!(o instanceof Match)) return false;

        Match other = (Match)o;
        return 	m_id == other.m_id
             && m_1 == other.m_1
             && m_2 == other.m_2
             && m_ts == other.m_ts
         ;
    }
    final public static ContentValues insertValues(int _1, int _2) {
        return insertValues(_1, _2, current());
    }
    final public static ContentValues insertValues(int _1, int _2, long ts) {
        ContentValues values = new ContentValues();
        values.put("_1", _1);
        values.put("_2", _2);
        values.put("ts", ts);
        return values;
    }
    final public ContentValues updateValues() {
        ContentValues values = insertValues(m_1, m_2, m_ts);
        values.put("id", m_id);
        return values;
    }
}