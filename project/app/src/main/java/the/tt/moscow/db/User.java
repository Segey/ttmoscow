package the.tt.moscow.db;

import android.content.ContentValues;
import java.util.Date;

public final class User {
    private int m_id = 0;
    private String m_name;
    private String m_surname;
    private Date m_date;
    private Byte[] m_photo;
    private int m_country = 1;

    public User(int id) {
        m_id = id;
    }
    public User(int id, String name) {
        this(id);
        m_name = name.trim();
    }
    public User(int id, String name, String surname) {
        this(id, name);
        m_surname = surname.trim();
    }
    public User(String name, String surname) {
        this(0, name, surname);
    }
    public boolean isNew() {
        return m_id == 0
           && !m_name.isEmpty();
    }
    public boolean isValid() {
        return m_id != 0
           && !m_name.isEmpty();
    }
    @Override
    public String toString() {
        return getClass().getName() + ": {"
                + "id: " + m_id + ", "
                + "name: " + m_name + ", "
                + "surnmae: " + m_surname + ", "
                + "country: " + m_country
        + "}";
    }
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof User))return false;

        User other = (User)o;
        return m_id == other.m_id
            && m_name.equals(other.m_name)
            && m_surname.equals(other.m_surname)
            && m_country == other.m_country
        ;
    }
    public int id() {
        return m_id;
    }
    public void setId(int id)  {
        m_id = id;
    }
    public String  name() {
        return m_name;
    }
    public void setName(String name)  {
        m_name = name;
    }
    public String surName() {
        return m_surname;
    }
    public void setSurname(String  surname) {
        m_surname = surname;
    }
    public Date birthDay()  {
        return m_date;
    }
    public void setBirthDay(Date date) {
        m_date = date;
    }
    public int country() {
        return m_country;
    }
    public void setCountry(int country) {
        m_country = country;
    }
    public Byte[] photo()  {
        return m_photo;
    }
    public void setPhoto(Byte[] photo)  {
        m_photo = photo;
    }
    final public ContentValues insertValues() {
        ContentValues values = new ContentValues();
        values.put("name", m_name);
        values.put("surname", m_surname);
        values.put("country", m_country);
        //values.put("photo", m_photo);
        if(m_date != null) values.put("birthday", m_date.toString());
        return values;
    }
    final public ContentValues updateValues() {
        ContentValues values = insertValues();
        values.put("id", m_id);
        return values;
    }
}
