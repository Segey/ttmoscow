package the.tt.moscow.db;

/**
 * Created by SPanin on 10/6/2016.
 */
final public class Game {
    private Player m_p1;
    private Player m_p2;
    private Match m_match;

    public Game(Player player1, Player player2, Match match) {
        this.m_p1 = player1;
        this.m_p2 = player2;
        this.m_match = match;
    }
    public void setPlayer1(Player player) {
        this.m_p1 = player;
    }
    public Player player1() {
        return m_p1;
    }
    public void setPlayer2(Player player) {
        this.m_p2 = player;
    }
    public Player player2() {
        return m_p2;
    }
    public void setMatch(Match match) {
        this.m_match = match;
    }
    public Match match() {
        return m_match;
    }
    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(o == this) return true;
        if(!(o instanceof Game)) return false;

        Game other = (Game)o;
        return m_p1.equals(other.m_p1)
            && m_p2.equals(other.m_p2)
            && m_match.equals(other.m_match)
        ;
    }
    @Override
    public String toString() {
        return getClass().getName() + ": {"
                + "\n\t" + m_p1
                + ",\n\t" + m_p2
                + ",\n\t" + m_match
                + "\n}";
    }
}