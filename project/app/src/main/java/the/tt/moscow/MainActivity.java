package the.tt.moscow;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import the.tt.moscow.user.UserActivity;

public class MainActivity extends ActionBarActivity {
    private static final String TAG = MainActivity.class.getCanonicalName();
    Button m_add_user = null;

    private final void instance() {
        instanceAddUser();
    }
    private final void instanceAddUser() {
        m_add_user = (Button)findViewById(R.id.m_add_user);
        m_add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), UserActivity.class));
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.users:
                startActivity(new Intent(getApplicationContext(), UserActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance();
    }
}
