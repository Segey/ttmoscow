package the.tt.moscow.user;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import the.tt.moscow.R;
import the.tt.moscow.adapter.TextAddapter;
import the.tt.moscow.db.Source;
import the.tt.moscow.db.User;
import the.tt.moscow.lib.StringList;
import the.tt.moscow.user.frame.UsersListFrame;

public class UserProfileActivity extends ActionBarActivity {
    private final static String TAG = UserProfileActivity.class.getCanonicalName();
    private Source m_source = null;
    private User m_user = null;
    private TextView l_name = null;
    private ListView m_stats = null;
    private TextAddapter m_adapter = null;

    private final void instance() {
        m_source = new Source(this);
        l_name  = (TextView)findViewById(R.id.l_name);
        m_stats = (ListView)findViewById(R.id.m_stats);
        instanceUser();
        instanceStats();
    }
    private final void instanceStats() {
        StringList values = new StringList();
        values.add("Androi", "2");
        values.add("WindowsMobile", "3");
        values.add("Max OS X", "4");

        m_adapter = new TextAddapter(this,
                android.R.layout.simple_list_item_2, new Integer[] {android.R.id.text1, android.R.id.text2}, values);
        m_stats.setAdapter(m_adapter);
    }
    private final void instanceUser() {
        final long id = getIntent().getLongExtra(UsersListFrame.ID_NAME, 0);
        m_user = m_source.user((int)id);

        if(!m_user.isValid())
            Log.w(TAG, String.format(" - Invalid user %s", m_user.toString()));
        l_name.setText(String.format("%s %s", m_user.surName(), m_user.name()));
    }
    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        instance();
    }
}
