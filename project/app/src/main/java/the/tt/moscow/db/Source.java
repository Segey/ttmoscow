package the.tt.moscow.db;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Date;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public final class Source extends SQLiteOpenHelper {
    private static final String TAG = Source.class.getCanonicalName();
    final static int DB_VERSION = 1;
    public final static String DB_NAME = "db.sqlite";
    final static String TABLE_USERS = "Users";
    final static String TABLE_SCORES = "Scores";
    final static String TABLE_MATCHES = "Matches";
    final Context m_context = null;

    private final String playersTable() {
        return new String("CREATE TABLE IF NOT EXISTS " + TABLE_USERS + "("
                + " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                + ",name TEXT NOT NULL"
                + ",surname TEXT"
                + ",birthday DATE"
                + ",country INTEGER default 1"
                + ",photo BLOB"
                + ");");
    }
    private final String scoresTable() {
        return new String("CREATE TABLE IF NOT EXISTS " + TABLE_SCORES + "("
                + " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                + ",pid INTEGER NOT NULL"
                + ",mid INTEGER NOT NULL"
                + ",score INTEGER NOT NULL"
                + ",FOREIGN KEY(pid) REFERENCES Players(id)"
                + ",FOREIGN KEY(mid) REFERENCES Matches(id)"
                + ");");
    }
    private final String matchesTable() {
        return new String("CREATE TABLE IF NOT EXISTS " + TABLE_MATCHES + "("
                + " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                + ",_1 INTEGER NOT NULL"
                + ",_2 INTEGER NOT NULL"
                + ", ts INTEGER NOT NULL"
                + ",FOREIGN KEY(_1) REFERENCES Players(id)"
                + ",FOREIGN KEY(_2) REFERENCES Players(id)"
                + ");");
    }
    private void insertPlayer(SQLiteDatabase db, String name, String surname, Date date) {
        db.execSQL(String.format("INSERT INTO \"Players\" (\"name\",\"surname\",\"birthday\") VALUES (%s,%s,%s)", name, surname, date.toString()));
    }
    private final Game game(String select) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = String.format("SELECT u.id, u.name, u.surname, m.id, m._1, m._2, m.ts, s.id, s.pid, s.mid, s.score "
                        + "FROM %s AS u INNER JOIN %s AS m "
                        + "  ON u.id == m._1 OR u.id == m._2 "
                        + "INNER JOIN %s AS s "
                        + "  ON s.mid == m.id AND s.pid = u.id "
                        + " WHERE %s"
                , TABLE_USERS, TABLE_MATCHES, TABLE_SCORES, select);

        Cursor cursor = db.rawQuery(query, null);
        if(cursor == null
                || !cursor.moveToFirst() || cursor.getCount()== 0) return null;

        User u1 = new User(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
        Match m = new Match(cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6));
        Score s1 = new Score(cursor.getInt(7), cursor.getInt(8), cursor.getInt(9), cursor.getInt(10));
        Player p1 = new Player(u1,s1);

        cursor.moveToNext();
        User u2 = new User(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
        Score s2 = new Score(cursor.getInt(7), cursor.getInt(8), cursor.getInt(9), cursor.getInt(10));
        Player p2 = new Player(u2,s2);

        return new Game(p1, p2, m);
    }
    public Source(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(playersTable());
        db.execSQL(matchesTable());
        db.execSQL(scoresTable());
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Players;");
        db.execSQL("DROP TABLE IF EXISTS Matches;");
        db.execSQL("DROP TABLE IF EXISTS Scores");
        onCreate(db);
    }
    public User user(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS, new String[] {"name", "surname", "country"}, " id = ?"
                , new String[]{String.valueOf(id)}, null, null, null);
        if(cursor == null) return null;

        cursor.moveToFirst();
        if(cursor.getCount()== 0) return null;

        User user = new User(id);
        user.setName(cursor.getString(0));
        user.setSurname(cursor.getString(1));
        user.setCountry(cursor.getInt(2));
        return user;
    }
    public Cursor allUsersCursor() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT id AS _id, name, surname FROM " + TABLE_USERS, null);
    }
    public final Cursor allCompletedGamesCursor(int id) {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery(MessageFormat.format("SELECT 1 AS _id, SUM(p1.score > p2.score) AS win , SUM(p2.score > p1.score) AS lose, COUNT(*) AS total"
                + " FROM {1} AS m JOIN {2} p1"
                + "   ON m.id = p1.mid AND p1.pid = {0}"
                + " JOIN {2} p2"
                + "   ON m.id = p2.mid AND p2.pid <> {0}"
                + " WHERE m._1 = {0} OR m._2 = {0};", id, TABLE_MATCHES, TABLE_SCORES)
                , null);
    }
    public boolean userExists(String name, String surname) {
        if(name.isEmpty() && surname.isEmpty()) return true;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS, new String[] {"name"}, " name = ? AND surname = ?"
                , new String[]{name, surname}, null, null, null);
        if(cursor == null) return false;
        cursor.moveToFirst();
        return cursor.getCount()!= 0;
    }
    public long addUser(User user) {
        SQLiteDatabase db = getWritableDatabase();
        long result = -1;
        try {
            db.beginTransaction();
            result = db.insert(TABLE_USERS, null, user.insertValues());
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }
    public final long editUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = -1;
        try {
            db.beginTransaction();
            result = db.update(TABLE_USERS, user.updateValues(), "id = ?", new String[]{String.valueOf(user.id())});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }
    public final long removeUser(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = -1;
        try {
            db.beginTransaction();
            result = db.delete(TABLE_USERS, "id = ?", new String[]{String.valueOf(id)});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }
    public final long removeAllUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = -1;
        try {
            db.beginTransaction();
            result = db.delete(TABLE_USERS, null, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }
    public final long editScore(Score score) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = -1;
        try {
            if(!score.isValid()) throw new IllegalArgumentException("The score isnot valid");
            db.beginTransaction();
            result = db.update(TABLE_SCORES, score.updateValues(), "id = ?", new String[]{String.valueOf(score.id())});
            db.setTransactionSuccessful();
        } catch (IllegalArgumentException e) {
            System.out.println(TAG + " " + e.toString());
        }
        finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }
    public final Score score(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_SCORES, new String[] {"pid", "mid", "score"}, " id = ?"
                , new String[]{String.valueOf(id)}, null, null, null);
        if(cursor == null
            || !cursor.moveToFirst() || cursor.getCount()== 0) return null;

        return new Score(id, cursor.getInt(0), cursor.getInt(1), cursor.getInt(2));
    }
    public final long editMatch(Match score) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = -1;
        try {
            if(!score.isValid()) throw new IllegalArgumentException("The score isnot valid");
            db.beginTransaction();
            result = db.update(TABLE_MATCHES, score.updateValues(), "id = ?", new String[]{String.valueOf(score.id())});
            db.setTransactionSuccessful();
        } catch (IllegalArgumentException e) {
            System.out.println(TAG + " " + e.toString());
        }
        finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }
    public final Match match(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_MATCHES, new String[] {"_1", "_2", "ts"}, " id = ?"
                , new String[]{String.valueOf(id)}, null, null, null);
        if(cursor == null
                || !cursor.moveToFirst() || cursor.getCount()== 0) return null;

        return new Match(id, cursor.getInt(0), cursor.getInt(1), cursor.getLong(2));
    }
    public final boolean addGame(int _1, int _2, int score1, int score2) {
        SQLiteDatabase db = getWritableDatabase();
        long mid = -1L;
        long result = -1L;
        try {
            db.beginTransaction();
            mid = db.insert(TABLE_MATCHES, null, Match.insertValues(_1, _2));
            if (mid == -1) throw new SQLException("Cannot insert a match");

            result = db.insert(TABLE_SCORES, null, Score.insertValues(_1, (int)mid, score1));
            if (result == -1) throw new SQLException("Cannot insert a score for first player");

            result = db.insert(TABLE_SCORES, null, Score.insertValues(_2, (int)mid, score2));
            if (result == -1) throw new SQLException("Cannot insert a score for second player");
            db.setTransactionSuccessful();
        } catch(SQLException e) {
            System.out.println(TAG + " " + e.toString());
        } finally {
            db.endTransaction();
            db.close();
        }
        return mid != -1 && result != -1;
    }
    public final Game game(int mid) {
        return game(String.format("m.id = %d;", mid));
    }
    public final Game game(GregorianCalendar date) {
        return game(String.format("date(m.ts / 1000,  'unixepoch') = '%s';", new SimpleDateFormat("yyyy-MM-dd").format(date.getTime())));
    }
}
