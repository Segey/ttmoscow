package the.tt.moscow.db;

/**
 * Created by SPanin on 10/10/2016.
 */
public class Player {
    private User m_user;
    private Score m_score;

    public Player(User user, Score score) {
        this.m_user = user;
        this.m_score = score;
    }
    public void setUser(User user) {
        this.m_user = user;
    }
    public User getUser() {
        return m_user;
    }
    public void setScore(Score score) {
        this.m_score = score;
    }
    public Score getScore() {
        return m_score;
    }
    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(o == this) return true;
        if(!(o instanceof Player)) return false;

        Player other = (Player)o;
        return 	m_user.equals(other.m_user)
                && m_score.equals(other.m_score)
        ;
    }
    @Override
    public String toString() {
        return getClass().getName() + ": {"
                + "user: " + m_user
                + ", score: " + m_score
                + "}";
    }
}