package the.tt.moscow.db;

import android.content.ContentValues;

/**
 * Created by SPanin on 10/5/2016.
**/
public final class Score {
    private int m_id;
    private int m_pid;
    private int m_mid;
    private int m_score;

    public Score(int id, int pid, int mid, int score) {
        this.m_id    = id;
        this.m_pid   = pid;
        this.m_mid   = mid;
        this.m_score = score;
    }
    public boolean isValid() {
        return m_id != 0
            && m_pid != 0
            && m_mid != 0
            && m_score >= 0
         ;
    }
    public void setId(int id) {
        this.m_id = id;
    }
    public int id() {
        return m_id;
    }
    public void setPid(int pid) {
        this.m_pid = pid;
    }
    public int pid() {
        return m_pid;
    }
    public void setMid(int mid) {
        this.m_mid = mid;
    }
    public int mid() {
        return m_mid;
    }
    public void setScore(int score) {
        this.m_score = score;
    }
    public int score() {
        return m_score;
    }
    @Override
    public String toString() {
        return getClass().getName() + ": {"
                + "id: " + m_id + ", "
                + "pid: " + m_pid + ", "
                + "mid: " + m_mid + ", "
                + "score: " + m_score
                + "}";
    }
    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if(o == this) return true;
        if(!(o instanceof Score)) return false;

        Score other = (Score)o;
        return 	m_id  == other.m_id
                && m_pid   == other.m_pid
                && m_mid   == other.m_mid
                && m_score == other.m_score
        ;
    }
    public static final ContentValues insertValues(int pid, int mid, int score) {
        ContentValues values = new ContentValues();
        values.put("pid", pid);
        values.put("mid", mid);
        values.put("score", score);
        return values;
    }
    public final ContentValues updateValues() {
        ContentValues values = new ContentValues();
        values.put("score", score());
        return values;
    }
}