package the.tt.moscow.user;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import the.tt.moscow.R;
import the.tt.moscow.db.Source;
import the.tt.moscow.db.User;

public class AddUserActivity extends ActionBarActivity implements View.OnKeyListener, View.OnClickListener {
    private final static String TAG = AddUserActivity.class.getCanonicalName();
    private int def_color = Color.TRANSPARENT;

    private EditText m_name    = null;
    private EditText m_surname = null;
    private Button   m_submit  = null;
    private TextView i_name    = null;
    private TextView i_surname = null;

    private final static String text(EditText view) {
        return view.getText().toString().trim();
    }
    private final void startAnim(TextView view) {
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.appear);
        view.clearAnimation();
        view.startAnimation(animation);
        view.setVisibility(View.VISIBLE);
    }
    private final boolean isUser() {
        Source source = new Source(getApplicationContext());
        return source.userExists(text(m_name) , text(m_surname));
    }
    private final void instance() {
        m_name = (EditText)findViewById(R.id.m_name);
        i_name = (TextView)findViewById(R.id.i_name);
        m_surname = (EditText)findViewById(R.id.m_surname);
        i_surname= (TextView)findViewById(R.id.i_surname);
        m_submit = (Button)findViewById(R.id.m_submit);
    }
    private final void init() {
        i_name.setVisibility(View.INVISIBLE);
        i_surname.setVisibility(View.INVISIBLE);

        m_name.setOnKeyListener(this);
        m_surname.setOnKeyListener(this);
        def_color = m_name.getCurrentTextColor();
        m_submit.setOnClickListener(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        instance();
        init();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        final boolean b = isUser();
        m_submit.setEnabled(!b);
        ((EditText)view).setTextColor(b ? Color.RED : def_color);

        updateInfoField(m_name, i_name);
        updateInfoField(m_surname, i_surname);
        return false;
    }
    private void updateInfoField(EditText text, TextView view) {
        if(text(text).isEmpty()) view.setVisibility(View.INVISIBLE);
        else
            if(view.getVisibility() == View.INVISIBLE)
                startAnim(view);
    }
    @Override
    public void onClick(View v) {
        if(isUser()) return;

        Source source = new Source(getApplicationContext());
        source.addUser(new User(text(m_name) , text(m_surname)));
        Toast.makeText(getApplicationContext(),
                String.format("New user '%s %s' has been added!", text(m_name) , text(m_surname)),
                Toast.LENGTH_SHORT).show();
        m_name.setText("");
        m_surname.setText("");
    }
}
