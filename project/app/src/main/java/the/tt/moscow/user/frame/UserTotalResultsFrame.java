package the.tt.moscow.user.frame;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListView;

import the.tt.moscow.R;
import the.tt.moscow.db.Source;
import the.tt.moscow.user.UserProfileActivity;

/**
 * Created by SPanin on 10/18/2016.
 **/
public final class UserTotalResultsFrame extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private SimpleCursorAdapter m_adapter = null;
    private Source m_source = null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText(getResources().getString(R.string.users));
        setHasOptionsMenu(true);

        m_source = new Source(getActivity());
        m_adapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_2, null,
                new String[] { "win", "lose", "total" },
                new int[] { android.R.id.text1, android.R.id.text2 }, 0);
        setListAdapter(m_adapter);
        getLoaderManager().initLoader(0, null, this);
    }
    //    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        MenuItem item = menu.add("Search");
//        item.setIcon(android.R.drawable.ic_menu_search);
//        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//        SearchView sv = new SearchView(getActivity());
//        item.setActionView(sv);
//    }
//    public boolean onQueryTextChange(String newText) {
//        m_filter = !TextUtils.isEmpty(newText) ? newText : null;
//        getLoaderManager().restartLoader(0, null, this);
//        return true;
//    }
    static final String[] CONTACTS_SUMMARY_PROJECTION = new String[] {
            "win",
            "lose",
            "total",
//            Contacts.DISPLAY_NAME,
//            Contacts.CONTACT_STATUS,
//            Contacts.CONTACT_PRESENCE,
//            Contacts.PHOTO_ID,
//            Contacts.LOOKUP_KEY,
    };
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new UserTotalResultsFrame.MyCursorLoader(getActivity(), m_source, id);
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.
//        Uri baseUri;
//        if (mCurFilter != null) {
//            baseUri = Uri.withAppendedPath(Contacts.CONTENT_FILTER_URI,
//                    Uri.encode(mCurFilter));
//        } else {
//            baseUri = Contacts.CONTENT_URI;
//        }
//
//        // Now create and return a CursorLoader that will take care of
//        // creating a Cursor for the data being displayed.
//        String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
//                + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
//                + Contacts.DISPLAY_NAME + " != '' ))";
        //return new CursorLoader(getActivity(), baseUri,
        //       CONTACTS_SUMMARY_PROJECTION, select, null,
        //       Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
    }

    public final void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        m_adapter.swapCursor(data);
    }
    public final void onLoaderReset(Loader<Cursor> loader) {
        m_adapter.swapCursor(null);
    }
    static class MyCursorLoader extends CursorLoader {
        Source m_db = null;
        int    m_id = 0;

        public MyCursorLoader(Context context, Source db, int id) {
            super(context);
            m_db = db;
            m_id = id;
        }
        @Override
        public Cursor loadInBackground() {
            return m_db.allCompletedGamesCursor(m_id);
        }
    }
}
