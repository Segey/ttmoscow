package the.tt.moscow;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import the.tt.moscow.db.Game;
import the.tt.moscow.db.Match;
import the.tt.moscow.db.Player;
import the.tt.moscow.db.User;
import the.tt.moscow.db.Score;
import the.tt.moscow.db.Source;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.util.Date;
import java.util.GregorianCalendar;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by SPanin on 10/4/2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DataBaseTest {
    private static final String TAG = DataBaseTest.class.getCanonicalName();
    private Source db = null;

    @Before
    public void setUp() throws Exception {
        ShadowApplication context = Shadows.shadowOf(RuntimeEnvironment.application);
        db = new Source(context.getApplicationContext());
        assertEquals(Source.DB_NAME, db.getDatabaseName());
    }
    @After
    public void tearDown() throws Exception {
        db.close();
    }
    @Test
    final public void ValidTableNumber() {
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type='table';", null);
        cursor.moveToFirst();
        assertEquals(cursor.getInt(0), 5);
    }
    @Test
    public void UserExists() {
        User _1 = new User("Michael", "Young");
        assertEquals(db.addUser(_1), 1L);
        _1.setId(1);

        assertTrue(db.userExists("", ""));
        assertTrue(db.userExists("Michael", "Young"));
        assertFalse(db.userExists("Sergey", "Young"));
    }
    @Test
    public void AddUser() {
        User _1 = new User("Michael", "Young");
        assertEquals(db.addUser(_1), 1L);
        _1.setId(1);

        User _2 = db.user(1);
        assertEquals(_1, _2);
    }
    @Test
    public void AddTwoUsers() {
        User _1 = new User("Michael", "Young");
        assertEquals(db.addUser(_1), 1L);

        User _2 = new User("Sergey", "Panin");
        assertEquals(db.addUser(_2), 2L);
    }
    @Test
    public void UpdateUser() {
        AddUser();

        User _1 = new User(1, "Sergey", "Panin");
        assertEquals(db.editUser(_1),1L);

        User _2 = db.user(1);
        assertEquals(_1, _2);
    }
    @Test
    public void RemoveUser() {
        AddUser();
        assertEquals(db.removeUser(1),1L);
    }
    @Test
    public void RemoveAllUsers() {
        AddUser();
        assertEquals(db.removeAllUsers(),1L);
    }
    @Test
    public void AddGame() {
        AddTwoUsers();
        assertTrue(db.addGame(1, 2, 11, 12));
    }
    @Test
    public void EditScore() {
        AddTwoUsers();
        assertTrue(db.addGame(1, 2, 11, 12));

        Score _1 = new Score(1, 1, 1, 33);
        assertEquals(db.editScore(_1), 1L);

        Score _2 = db.score(1);
        assertEquals(_1, _2);
    }
    @Test
    public void EditMatchTime() {
        AddTwoUsers();
        assertTrue(db.addGame(1, 2, 11, 12));

        Match _1 = new Match(1, 1, 2);
        assertEquals(db.editMatch(_1), 1L);

        Match _2 = db.match(1);
        assertEquals(_1, _2);
    }
    @Test
    public void GameById() {
        AddTwoUsers();
        assertTrue(db.addGame(1, 2, 11, 12));

        Game game = db.game(1);
        assertNotNull(game);

        Match match = db.match(1);
        assertEquals(match, game.match());

        Player p1 = new Player(db.user(1), db.score(1));
        Player p2 = new Player(db.user(2), db.score(2));

        assertEquals(p1, game.player1());
        assertEquals(p2, game.player2());
    }
    @Test
    public void GameByDate() {
        AddTwoUsers();
        assertTrue(db.addGame(1, 2, 11, 12));

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(new Date(System.currentTimeMillis()));

        Game game = db.game(cal);
        assertNotNull(game);

        Match match = db.match(1);
        assertEquals(match, game.match());

        Player p1 = new Player(db.user(1), db.score(1));
        Player p2 = new Player(db.user(2), db.score(2));

        assertEquals(p1, game.player1());
        assertEquals(p2, game.player2());
    }
}