package the.tt.moscow;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import java.util.ArrayList;
import the.tt.moscow.lib.StringList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by SPanin on 10/17/2016.
**/
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class StringListTest {
    @Test
    final public void Invalid() {
        StringList list = new StringList();
        assertEquals(list.size(), 0);
        assertTrue(list.isEmpty());
    }
    @Test
    final public void Add() {
        StringList list = new StringList();
        list.add("Cool", "Bool");

        assertEquals(list.size(), 1);
        assertTrue(!list.isEmpty());

        for(ArrayList<String> s: list) {
            assertEquals(s.get(0), "Cool");
            assertEquals(s.get(1), "Bool");
        }
    }
}
