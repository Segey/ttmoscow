package the.tt.moscow;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import the.tt.moscow.db.Match;

/**
 * Created by SPanin on 10/6/2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public final class MatchDateTest {
    @Test
    public void DateTest() {
        Match match = new Match(1, 1, 1, 1475759913515L);
        assertEquals(match.getDay(), 6);
        assertEquals(match.getMonth(), 9);
        assertEquals(match.getYear(), 2016);
    }
}
